'use strict';


angular.module('smartHome')
  .controller('EventsCtrl', ['$routeParams', '$location', '$scope', '$http', function($routeParams, $location, $scope, $http) {
    $scope.url = 'http://localhost:3000/users/events/';
    $scope.events='';

    $scope.getEvents = function () {
      $http.get($scope.url+'getAll')
        .success(function(response) {
          $scope.events=response;
          console.log(response);
        })
        .error(function(err) {
          console.log(err);
        });
    };

    $scope.init=function(){
      $scope.getEvents();
    };

    $scope.delete = function(event){

      console.log(event.id);
      $http.delete($scope.url+'delete/'+event.id)
        .success(function(data){
          console.log(data);
          $scope.events = data;
        })
        .error(function(err) {
          console.log(err);
        });

    }
  }]);
