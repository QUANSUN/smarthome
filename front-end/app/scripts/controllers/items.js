'use strict';


angular.module('smartHome')
  .controller('ItemsCtrl', ['$routeParams', '$location', '$scope', '$http', function($routeParams, $location, $scope, $http) {

    $scope.url = 'http://localhost:3000/users/items/getAll';

    $scope.init = function(){

      $http.get($scope.url)
        .success(function(response) {
          console.log(response);
          $scope.items = response;
        })
        .error(function(err) {
          console.log(err);
        });
    };

  }]);
