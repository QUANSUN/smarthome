'use strict';

function ISODateString(d){
  function pad(n){return n<10 ? '0'+n : n};

  return d.getUTCFullYear()+'-' + pad(d.getUTCMonth()+1)+'-'+ pad(d.getUTCDate())+'T'+ pad(d.getUTCHours())+':'
    + pad(d.getUTCMinutes()) +':' + pad(d.getUTCSeconds())+'+02:00';
};


function convert(event, callback){
  var d = new Date();
  var d2 = new Date();
  if(event.date) {
    d.setYear(event.date.getFullYear());
    d.setMonth(event.date.getMonth());
    d.setDate(event.date.getDate());
    d.setHours(event.timeStart.hh+2);
    d.setMinutes(event.timeStart.mm);

    d2.setYear(event.date.getFullYear());
    d2.setMonth(event.date.getMonth());
    d2.setDate(event.date.getDate());
    d2.setHours(event.timeEnd.hh+2);
    d2.setMinutes(event.timeEnd.mm);
  }

  event.start = {
    'dateTime' : ISODateString(d),
    'timeZone' : 'GMT+01:00'
  };

  event.start.dateTime= ISODateString(d);
  event.end = {
    'dateTime' : ISODateString(d2),
    'timeZone' : 'GMT+01:00'
  };
  event.end.dateTime = ISODateString(d2);

  event.description = event.summary;
  event.email = 'user@mail.com';
  callback(event);
};

angular.module('smartHome')
  .controller('CreateEvtCtrl', ['$routeParams', '$location', '$scope', '$http',function($routeParams, $location, $scope, $http) {
    $scope.url = 'http://localhost:3000/users/createEvent';
    $scope.events='';
    $scope.response='';

    $scope.hMsg = false;
    $scope.hErr = false;

    $scope.clean = function(){
      $scope.hMsg = false;
      $scope.hErr = false;
    };

    //http://localhost:3000/users/createEvent
    $scope.createEvent = function(event){

      $scope.hMsg = false;
      $scope.hErr = false;

      convert(event, function(data){
        console.log(data);

        $http.post($scope.url, data)
          .success(function(data2){
            console.log(data2);
            $scope.hMsg = true;
            $scope.hErr = false;
            $scope.response = data2;
          }).error(function(err){
            console.log(err);
            $scope.hErr = true;
            $scope.hMsg = false;
          });
      });
    }
  }]);

