/**
 * Created by user on 09/06/15.
 */
//CytoscapeCtrl
'use strict';


/*
 This demo visualises the railway stations in Tokyo (東京) as a graph.

 This demo gives examples of

 - loading elements via ajax
 - loading style via ajax
 - using the preset layout with predefined positions in each element
 - using motion blur for smoother viewport experience
 - using `min-zoomed-font-size` to show labels only when needed for better performance
 */

angular.module('smartHome')
  .controller('CytoscapeCtrl', ['$routeParams', '$location', '$scope', '$http',function($routeParams, $location, $scope, $http, socket) {

    $scope.url = 'http://10.212.103.253:3000/api/path';


    $scope.elements={
      nodes: [
        { data: { id: '1', name: 'Réveiller' } },
        { data: { id: '2', name: 'L2' } },
        { data: { id: '3', name: 'L3' } },
        { data: { id: '4', name: 'Sortir' } }
      ],
      edges: [
        {data: {source: '1', target: '2'}},
        {data: {source: '2', target: '3'}},
        {data: {source: '3', target: '4'}}
      ]
    };

    $scope.init = function(){
      $scope.getPath();
    };

    $scope.getPath = function(){
      $http.get($scope.url + '/construct_graph')
        .success(function (response) {
          $scope.elements = response;
          $scope.graph4();
        })
        .error(function (err) {
          console.log(err);
        });
    };

    $scope.graph1 = function() {
      $scope.cy = cytoscape({
        container: $('#cy')[0],

        style: cytoscape.stylesheet()
          .selector('node')
          .css({
            'content': 'data(name)',
            'text-valign': 'center',
            'color': 'white',
            'text-outline-width': 2,
            'text-outline-color': '#888'
          })
          .selector(':selected')
          .css({
            'background-color': 'black',
            'line-color': 'black',
            'target-arrow-color': 'black',
            'source-arrow-color': 'black',
            'text-outline-color': 'black'
          })
          .selector('edge')
          .css({
            'target-arrow-shape': 'triangle'
          }),

        elements: $scope.elements,
        layout: {
          name: 'grid',
          padding: 10
        }
      });

      $scope.cy.on('tap', 'node', function () {
        try { // your browser may block popups
          window.open(this.data('href'));
        } catch (e) { // fall back on url change
          window.location.href = this.data('href');
        }
      });
    };

    $scope.graph2 = function() {
      $scope.cy = cytoscape({
        container: document.getElementById('cy'),

        style: [
          {
            selector: 'node',
            css: {
              'content': 'data(name)',
              'text-valign': 'center',
              'text-halign': 'center'
            }
          },
          {
            selector: '$node > node',
            css: {
              'padding-top': '10px',
              'padding-left': '10px',
              'padding-bottom': '10px',
              'padding-right': '10px',
              'text-valign': 'top',
              'text-halign': 'center'
            }
          },
          {
            selector: 'edge',
            css: {
              'target-arrow-shape': 'triangle'
            }
          },
          {
            selector: ':selected',
            css: {
              'background-color': '#f00',
              'line-color': '#F00',
              'target-arrow-color': 'black',
              'source-arrow-color': 'black'
            }
          }
        ],

      elements:  $scope.elements,

      layout: {
        name: 'cose',
        padding: 5
      }
    });
    };


    $scope.graph3 = function() {
      $scope.cy = cytoscape({
        container: $('#cy')[0],

        style: cytoscape.stylesheet()
          .selector('node')
          .css({
            'content': 'data(name)',
            'text-valign': 'center',
            'color': 'white',
            'text-outline-width': 2,
            'text-outline-color': '#888'
          })
          .selector(':selected')
          .css({
            'background-color': '#a00',
            'line-color': 'red',
            'target-arrow-color': 'black',
            'source-arrow-color': 'black',
            'text-outline-color': 'black'
          })
          .selector('edge')
          .css({
            'target-arrow-shape': 'triangle'
          }),

        elements:  $scope.elements,

        layout: {
          name: 'grid',
          padding: 10
        }
      });
    };

    $scope.graph4 = function() {
      $scope.cy = cytoscape({
        container: document.getElementById('cy'),

        style: [
          {
            selector: 'node',
            css: {
              'content': 'data(name)',
              'text-valign': 'center',
              'color': 'white',
              'text-outline-width': 2,
              'text-outline-color': '#888'
            }
          },
          {
            selector: '$node > node',
            css: {
              'padding-top': '10px',
              'padding-left': '10px',
              'padding-bottom': '10px',
              'padding-right': '10px',
              'text-valign': 'top',
              'text-halign': 'center'
            }
          },
          {
            selector: 'edge',
            css: {
              'target-arrow-shape': 'triangle'
            }
          },
          {
            selector: ':selected',
            css: {
              'background-color': 'black',
              'line-color': 'black',
              'target-arrow-color': 'black',
              'source-arrow-color': 'black'
            }
          }
        ],

        elements:  $scope.elements,

        layout: {
          name: 'cose',
          padding: 5
        }
      });
    };


  }]);

