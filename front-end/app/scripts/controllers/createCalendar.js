'use strict';


angular.module('smartHome')
  .controller('CreateCalendarCtrl', ['$routeParams', '$location', '$scope', '$http', function($routeParams, $location, $scope, $http) {
      $scope.url = 'http://localhost:3000/calendars/createCalendar';
      $scope.createCalendar = function(ressource) {
        console.log(ressource);
        $http.post($scope.url, ressource)
        .success(function (newCalendar) {
          console.log(newCalendar.message);
          $scope.newCalendar = newCalendar.message;
        })
        .error(function (err) {
          console.log(err);
        });
      }

      $scope.master = {};
    $scope.event = {};
    $scope.res = {};

    $scope.create = function (event) {
      $scope.master = angular.copy(event);

    };

    $scope.reset= function () {
      $scope.event = angular.copy($scope.master);
    }
    }]);

