'use strict';


angular.module('smartHome')
  .controller('CalendarsCtrl', ['$routeParams', '$location', '$scope', '$http', function($routeParams, $location, $scope, $http) {
      $scope.url = 'http://localhost:3000/calendars/createCalendar';
      $scope.init= function () {

      }
      $scope.createCalendar = function(ressource) {
        $http.post($scope.url, ressource)
        .success(function (newCalendar) {
          console.log(newCalendar.message);
          $scope.newCalendar = newCalendar.message;
        })
        .error(function (err) {
          console.log(err);
        });
      }
    }]);

