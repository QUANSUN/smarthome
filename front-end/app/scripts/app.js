'use strict';

angular
  .module('smartHome', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    // 3rd party dependencies
    'btford.socket-io'
  ])
  .config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller:  'MainCtrl'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller:  'UsersCtrl'
      })
      .when('/usersV2', {
        templateUrl: 'views/usersV2.html',
        controller:  'UsersV2Ctrl'
      })
      .when('/createEvent', {
        templateUrl: 'views/createEvent.html',
        controller:  'CreateEvtCtrl'
      })
      .when('/createAction', {
        templateUrl: 'views/createAction.html',
        controller:  'CreateActionCtrl'
      })
      .when('/createCalendar', {
        templateUrl: 'views/createCalendar.html',
        controller:  'CreateCalendarCtrl'
      })
      .when('/users/items', {
        templateUrl: 'views/items.html',
        controller:  'ItemsCtrl'
      })
      .when('/users/events', {
        templateUrl: 'views/events.html',
        controller:  'EventsCtrl'
      })
      .when('/users/calendars', {
        templateUrl: 'views/calendars.html',
        controller:  'CalendarsCtrl'
      })
      .when('/users/cytoscape', {
        templateUrl: 'views/cytoscape.html',
        controller:  'CytoscapeCtrl'
      })
      .when('/users/realTime', {
        templateUrl: 'views/realTime.html',
        controller:  'RealTimeCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
