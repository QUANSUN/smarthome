#Linux

##Install yo, grunt-cli, bower, generator-angular and generator-karma :

 - sudo npm install -g grunt-cli bower yo generator-karma generator-angular

##Make a new directory, and cd into it :

 - mkdir front-end && cd $_
 
##Télécharger le ficher
 - https://drive.google.com/open?id=0ByLaaJ5BS8DIQ3JOeUh1RnZJYmM&authuser=0

 - Extract here

 - npm install

##Build
 - grunt

##Exexecution
 - grunt serve
