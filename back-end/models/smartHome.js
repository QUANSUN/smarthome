/**
 * Created by tom on 07/06/15.
 */

var calendar = require('../models/calendars');
var events = require('../models/events');



function computeHomeSchedule(req,res, callback)
{
    events.getAllEvents(req, res,
        function(err, data){
          if(err) {
            res.json(err);
          }
          else{
            var cal = new calendar.Calendar(data.summary,data.items)
            var calProcessor = new calendar.CalendarTraductor(cal, new calendar.Calendar())
            calProcessor.process();
            for(var i=0 ; i< calProcessor.homeCalendar.events.length ; ++i) {
              events.outerPostEvent(req,res,calProcessor.homeCalendar.events[i]);

            }
            callback(null, data);
          }
        });
}


module.exports.computeHomeSchedule = computeHomeSchedule;