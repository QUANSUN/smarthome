

var google = require('googleapis');
var calendar = google.calendar('v3');
var fs = require('fs');

var auth = require('../middlewares/auth');


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Get all events                                            #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

function getEvents(auth, req, callback){
    calendar.events.list({
            auth: auth,
            calendarId: 'primary',
            // timeMin: (new Date()).toISOString(),
            //maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime'
        },
        function(err, response) {
            if (err) {
                console.log('There was an error contacting the Calendar service: ' + err);
                callback(err, response);
            }
            var events = response.items;
            callback(null, response);

        }
    );
}

function getAllEvents(req,res, callback){
    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            res.json(err);
        }
        // Authorize a client with the loaded credentials, then call the Calendar API.
        auth.authorize2(JSON.parse(content), getEvents, req, callback);
    });
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Event Creation                                            #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


function  postEventCreation(oauth2Client, event, callback) {


    calendar.events.insert({
        auth: oauth2Client,
        calendarId: "primary",
        resource: event
    }, function (err, something) {
        if (err) {
            console.log(err);
            callback(err, something);
        } else {
            callback(null, something);
        }
    });
}

function createEvent(req,res, callback) {

    var start = req.param('start');
    var end = req.param('end');

    var d = req.param('start');
    var dataTimeS = d;
    d = req.param('end');
    var dataTimeE = d;
    var summary = req.param('summary');
    var location = "In your smart home!";
    var description = req.param('description');
    var email = req.param('email');



    var event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': start,
        'end': end,
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=1'
        ],
        'attendees': [
            {'email': email},
            {'email': email},
        ],
        'reminders': {
            'useDefault': false,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'sms', 'minutes': 10},
            ]
        }
    };

    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            res.error(err);
        }
        // Authorize a client with the loaded credentials, then call the Calendar API.
        auth.authorize2(JSON.parse(content), postEventCreation, event, callback);
    });
}

function outerPostEvent(req, res, event){
    fs.readFile('client_secret.json', function (err, content) {
        if (err) {
            res.json(err);
        }
        else {
            auth.authorize2(JSON.parse(content), postEventCreation, event, function (err, data) {
                if (err){
                    res.json(err);
                }
            });
        }
    });
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Delete an event                                           #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

function eventDelete(auth, event_id, callback){
    calendar.events.delete({
        auth: auth,
        calendarId: "primary",
        eventId: event_id
    }, function (err, something) {
        if (err) {
            console.log(err);
            callback(err, something);
        } else {
            console.log("OKOKOK");
            callback(null, something);
        }
    });
}

function deleteEvent(event_id, callback){
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            callback(err, null);
        }
        // Authorize a client with the loaded credentials, then call the Calendar API.
        auth.authorize2(JSON.parse(content), eventDelete, event_id, callback);
    });
}

module.exports.postEventCreation = postEventCreation;
module.exports.deleteEvent = deleteEvent;
module.exports.getAllEvents = getAllEvents;
module.exports.createEvent = createEvent;
module.exports.outerPostEvent = outerPostEvent;