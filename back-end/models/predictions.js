/**
 * Created by tom on 10/06/15.
 */

var Path = require('../bd/models/path');


function getPathsBeginningWith(beginning, treatment, callback){
    //console.log("getPathsBeginningWith : Call");
    var selectedPaths = [];
    Path.find(null,function(err,data){
        var doneActions=beginning.actions;
        data.forEach(function(element, index, array){
            //console.log('### : Comparing ' + element.actions + ' with ' + doneActions);
            var beginWith = true;
            doneActions.every(function(elt, ind, arr){
                //console.log('# : Comparing ' + element.actions[ind] + ' with ' + elt);
                if ( element.actions[ind] != elt){
                    beginWith=false;
                    return false;
                } else return true;
            });
            if(beginWith) selectedPaths.push(element);
        })
        //console.log("getPathsBeginningWith : " + selectedPaths.length);
        treatment(selectedPaths,beginning, callback);

    })
}

function getBestPathBeginningWith(beginning,nextAction,callback){
    getPathsBeginningWith(beginning,function(selection){
        var selectedPath;
        var index = beginning.actions.length;
        selection.forEach(function(elt, ind, arr){
            if(!selectedPath || (elt.actions[index] == nextAction.name && selectedPath.rank<elt.rank))
                selectedPath = elt;
        });
        callback(null, selectedPath);
    })
}


function selectBestAction(paths, currentPath, callback) {
    //console.log("selectBestPath : Call");
    var nextPossibleActions={};
    var index = currentPath.actions.length;
    //console.log("CURRENT path : "+currentPath.actions);
    paths.forEach(function(element,ind,array){
     //   console.log(element);
        var action = element.actions[index];
        if(!nextPossibleActions[action])
            nextPossibleActions[action]=0;
        nextPossibleActions[action]+=element.rank;
    });
    //console.log("selectBestPath : ");
    //console.log(nextPossibleActions);
    if (nextPossibleActions.length == 1){
        return nextPossibleActions[0];
    }else{
        var firstAction = {
            'name' : null,
            'value' : 0
        }
        var secondAction = firstAction;
        for(action in nextPossibleActions){
            console.log(action + ' : ' + nextPossibleActions[action]);
            if(nextPossibleActions[action]>= firstAction.value) {
                secondAction = firstAction;
                firstAction.name = action;
                firstAction.value = nextPossibleActions[action];
            }
        }
        callback(null, firstAction)

    }
}

function selectBestPath(path, currentPath, callback){
    selectBestAction(path, currentPath, function(err, data){
        getBestPathBeginningWith(currentPath,data,callback);
    })
}

function predictAction(currentPath, callback){
    console.log("on predit l'action :");
    getPathsBeginningWith(currentPath, selectBestAction, callback);
}

function predictPath(currentPath, callback){
    console.log("on predit le chemin :");
    getPathsBeginningWith(currentPath, selectBestPath, callback);
}
module.exports.predictAction = predictAction;
module.exports.predictPath = predictPath;