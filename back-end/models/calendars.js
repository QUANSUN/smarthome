/**
 * Created by tom on 07/06/15.
 */




// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Calendar                                                  #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


function Calendar(id,events){
    this.id = id;
    this.events = events || [];
    this.name="JE SUIS UN CALENDRIER INTELLIGENT";

}

Calendar.prototype.addEvent = function(event){
    this.events.push(event);
}




// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Calendar processor                                        #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


var RECOGNIZED_ACTIONS = {
    'Repas' : {
        begin : ['Light_GF_Kitchen_Ceiling ON', 'Light_GF_Kitchen_Table ON'],
        end : ['Light_GF_Kitchen_Ceiling OFF', 'Light_GF_Kitchen_Table OFF']
    },
    'Depart' : {
        begin : ['Lights OFF']
    },
    'Arrive' : {
        begin : ['Light_GF_Corridor_Ceiling ON']
    },
    'Dormir' : {
        begin : ['Lights OFF'],
        end : ['Light_FF_Bed_Ceiling ON', 'Light_FF_LivingRoom_GreenLight ON']
    }
}

var convertDescription = function(description){
    var descr = "";
    if(description.begin){
        descr+='start {\n';
        for(var i=0 ; i< description.begin.length ; ++i){
            descr+='\tsend '+description.begin[i]+'\n';
        }
        descr+='}'
    }

    if(description.end){
        descr+='end {\n';
        for(var i=0 ; i< description.end.length ; ++i){
            descr+='\tsend '+description.end[i]+'\n';
        }
        descr+='}'
    }

    return descr;

}

function CalendarTraductor(usersCalendar, homeCalendar) {
    this.usersCalendar = usersCalendar;
    this.homeCalendar = homeCalendar;

}


CalendarTraductor.prototype.process = function(){
    for(var i=0 ; i<this.usersCalendar.events.length ; ++i){
        var event = this.usersCalendar.events[i];

        if(RECOGNIZED_ACTIONS[event.description]){
            var newAction = RECOGNIZED_ACTIONS[event.description];
            event.description = convertDescription(newAction);
            //console.log(event.description)

            event.start.timeZone = 'UTC+01:00';
            event.end.timeZone = 'UTC+01:00';

            var event2=event;
            event2.id="";
            event2.iCalUID="";

            this.homeCalendar.addEvent(event2);
            console.log(event2);
        }
    }
}


exports.Calendar = Calendar;
exports.CalendarTraductor  = CalendarTraductor;
