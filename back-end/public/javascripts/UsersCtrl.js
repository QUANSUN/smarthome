/**
 * Created by user on 02/06/15.
 */

var phonecatApp = angular.module('smartHome', []);

/* use a function for the exact format desired... */
function ISODateString(d){
    function pad(n){return n<10 ? '0'+n : n}

    return d.getUTCFullYear()+'-'
        + pad(d.getUTCMonth()+1)+'-'
        + pad(d.getUTCDate())+'T'
        + pad(d.getUTCHours())+':'
        + pad(d.getUTCMinutes())+':'
        + pad(d.getUTCSeconds())+'Z';
}

phonecatApp.controller('UsersCtrl', function ($scope) {

    $scope.master = {};

    $scope.update = function(event) {
        $scope.master = angular.copy(event);
    };

    $scope.reset = function() {
        $scope.event = angular.copy($scope.master);
    };

    $scope.reset();
});
