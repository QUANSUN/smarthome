/**
 * Created by tom on 03/06/15.
 */


angular.module('smartHome', []).controller('ItemsCtrl', function ($scope, $http) {

    $scope.url = '/users/items';

    $scope.init=function(){


        $http.get($scope.url + '/getAll', {
            withCredentials: true
        })
            .success(function(response) {
                $scope.items=response;
                console.log(response);
            })
            .error(function(response) {
                console.log("Not logged!");
            });
    }


});
