var express = require('express');
var router = express.Router();
var Path = require('../bd/models/path');
var TempActions = require('../bd/models/tempActions');


// middleware to use for all requests
router.use(function(req, res, next) {// do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

function addTimeIntervals(timings, absoluteTimes) {
   
    for(i = 0; i < (absoluteTimes.length-1); i++) {
        if(timings[i] == null) {
            timings[i] = [];
        }
        var interval = ((absoluteTimes[i+1].getTime() - absoluteTimes[i].getTime())/1000);
        timings[i].push(interval);
    }

    return timings;
}

function saveNewPath(tempTable, callback) {
    var path = new Path();      // create a new instance of the item model
    path.actions = tempTable.actions;
    path.start_times = tempTable.absoluteTimes[0];
    path.timings = addTimeIntervals(path.timings, tempTable.absoluteTimes);
    path.rank = 1;
    // save the item and check for errors
    path.save(function(err, obj) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, obj);
        }
    });
}

function updatePath(tempTable, path, callback){
    path.start_times.push(tempTable.absoluteTimes[0]);
    path.timings = addTimeIntervals(path.timings, tempTable.absoluteTimes);
    path.rank += 1;
    // save the item and check for errors
    path.update({"timings" : path.timings, "start_times" : path.start_times, "rank" : path.rank}, function(err, obj) {
        callback(err, obj);
    });
}

function addNewActions(tempTable) {
    Path.findOne({ 'actions' : tempTable.actions }, function(err, result) {
        if (err) {
            console.log(err);
        } else {
            if(result == null) {
                saveNewPath(tempTable, function(err) {
                    if(err) {
                        console.log(err);
                    } else {
                        console.log('Path created!');
                    }
                });
            } else {
                updatePath(tempTable, result, function(err, r){
                    if(err) {
                        console.log(err);
                    } else {
                        console.log('Path updated!');
                    }
                });
            }
        }
    });
}

router.route('/')
    .post(function(req, res) {
        var temp = {
            "actions" : ['a', 'c', 'd', 'e'],
            "absoluteTimes" : [new Date("October 13, 2014 11:13:00"), new Date("October 13, 2014 11:13:11"), new Date("October 13, 2014 11:13:20"), new Date("October 13, 2014 11:13:25")]
        };

        Path.findOne({ 'actions' : temp.actions }, function(err, result) {
            if (err) {
                res.json(err);
            } else {
                if(result == null) {
                    saveNewPath(temp, function(err) {
                        if(err) {
                            res.json(err);
                        } else {
                            res.json( { message : 'Path created!' } );
                        }
                    });
                } else {
                    updatePath(temp, result, function(err, r){
                        if(err) {
                            res.json(err);
                        } else {
                            res.json( { message : 'Path updated!' } );
                        }
                    });
                }
            }
        });
    })
    .get(function(req, res) {
        Path.find(function(err, items) {
            if (err)
                res.send(err);

            res.json(items);
        });
    });

function constructGraph(path_list, callback) {
    console.log("Creating the graph. Wait...");

    var nodes = [];
    var edges = [];

    for(var i = 0; i < path_list.length; i++) {
        var actions = path_list[i].actions;
        for(var j = 0; j < actions.length; j++) {
            // add a graph node
            nodes.push({ 
                data : {
                    id : actions[j],
                    name : actions[j]
                }
            });
            // add a graph edge
            if(j > 0) {
                edges.push({
                    data : {
                        id : actions[j-1]+actions[j],
                        source : actions[j-1],
                        target : actions[j]
                    }
                });
            }
        }
    }

    var elements = {
        "nodes" : nodes,
        "edges" : edges
    };

    callback(elements);
}

router.route('/construct_graph')
    .get(function(req, res, next) {
        Path.find({}, function(err, path_list) {
            if(err) {
                res.json(err);
            } else {
                if(path_list != null) {
                    constructGraph(path_list, function(err, path_list) {
                            if(err) {
                                res.json(err);
                            } else {
                                res.json( { message : 'Graph constructed!' } );
                            }
                    });
                } else {
                    res.json("Error! There is no path!");
                }
            }
        });
    });

router.route('/get_current_table')
    .get(function(req, res, next) {
        TempActions.findOne({}, {}, { sort : { 'created_at' : -1 } }, function(err, table) {
            if(err) {
                res.json(err);
            } else {
                if(table == null) {
                    res.json( { error : 'no actions identified' }, 404 );
                } else {
                    constructGraph([table], function(response){
                        res.json(response);
                    });
                }
            }
        });
    });

router.route('/:item_id')

    // get the item with that id (accessed at GET http://localhost:8080/api/items/:item_id)
    .get(function(req, res) {
        Item.findById(req.params.item_id, function(err, item) {
            if (err)
                res.send(err);
            res.json(item);
        });
    })
    // update the item with this id (accessed at PUT http://localhost:8080/api/items/:item_id)
    .put(function(req, res) {

        // use our item model to find the item we want
        Item.findById(req.params.item_id, function(err, item) {

            if (err)
                res.send(err);

            item.name = req.body.name;  // update the items info
            item.status = req.body.status;
            item.date = req.body.date;
            // save the item
            item.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'item updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Item.remove({
            _id: req.params.item_id
        }, function(err, item) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });


module.exports = router;
module.exports.addNewActions = addNewActions;
