"use strict";

var express = require('express');
var router = express.Router();

var fs = require('fs');
var auth = require('../middlewares/auth')
var events = require('../models/events');
var myCalendar = require('../models/calendars');
var smartHome = require('../models/smartHome');

var predictions = require('../models/predictions');


var tempAction = require('../bd/models/tempActions');



router.post('/createEvent', function(req, res, next) {
  events.createEvent(req, res, function(err, data){
    if (err)
      res.json(err);
    else{
      res.json(data);
    }

  });
});

/* GET users listing. */
router.get('/events/getAll', function(req, res, next) {
  events.getAllEvents(req, res, function(err, data){
    if(err)
      res.json(err);
    else
      res.json(data);
  });
});

router.delete('/events/delete/:event_id', function(req, res, next) {
  events.deleteEvent(req.params.event_id, function(err, data){
    if(err)
      res.json(res);
    else
      events.getAllEvents(req, res, function(err, data){
        if(err) res.json(err);
        else res.json(data);
      });
  });

});


router.get('/createCalendar', function(req, res, next) {
  res.render('createCalendar', { title: 'Smart Home'});
});



router.get('/automate', function(req,res,next){
  smartHome.computeHomeSchedule(req,res, function(err, data){
    if(err){
      res.json(err);
    }else{
      res.json(data.summary);
    }
  });

});

router.get('/predict', function(req,res,next){

  tempAction.find(null, function(err, data){
    if(!data[0])
      res.json("No current tracking.");
    else predictions.predictPath(data[0], function(err, data2){
          if(err)
            res.json(err);
          else if (data2)
            res.json(data2);
          else res.json('No prediction avaible.');
        }
    )
  });

});

router.get('/', function(req, res, next) {
  res.render('users', { title: 'Smart Home'});
});

module.exports = router;
