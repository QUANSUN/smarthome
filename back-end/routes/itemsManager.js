var express = require('express');
var router = express.Router();
var Item = require('../bd/models/item');

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

router.route('/')
    .post(function(req, res) {

        var item = new Item();      // create a new instance of the item model
        item.name = req.body.name;  // set the items name (comes from the request)
        item.status = req.body.status;
        item.date = req.body.date;
        // save the item and check for errors
        item.save(function(err, b) {
            if (err)
                res.json(err);
            res.json({ message: 'item created!' });
        });
    })
    .get(function(req, res) {
        Item.find(function(err, items) {
            if (err)
                res.send(err);

            res.json(items);
        });
    });

router.route('/:item_id')

    // get the item with that id (accessed at GET http://localhost:8080/api/items/:item_id)
    .get(function(req, res) {
        Item.findById(req.params.item_id, function(err, item) {
            if (err)
                res.send(err);
            res.json(item);
        });
    })
    // update the item with this id (accessed at PUT http://localhost:8080/api/items/:item_id)
    .put(function(req, res) {

        // use our item model to find the item we want
        Item.findById(req.params.item_id, function(err, item) {

            if (err)
                res.send(err);

            item.name = req.body.name;  // update the items info
            item.status = req.body.status;
            item.date = req.body.date;
            // save the item
            item.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'item updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Item.remove({
            _id: req.params.item_id
        }, function(err, item) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });


module.exports = router;