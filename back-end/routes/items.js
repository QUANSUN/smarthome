/**
 * Created by tom on 03/06/15.
 */
var express = require('express');
var router = express.Router();
var http = require('http');
var xml2js = require('xml2js');
var Item = require('../bd/models/item');
var predictions = require('../models/predictions');
var tempAction = require('../bd/models/tempActions');
var pathManager = require('./pathManager');

function getAllItems(res){
    var options = {
        host:'localhost',
        port:'8080',
        path:'/rest/items',
        method: 'GET'
    }
    http.get(options,
        function(resp) {
            resp.setEncoding('utf8');
            var itemsXML= "";
            resp.on('data',
                function (chunk) {
                    console.log("CHUNCK RECIEVED : " + typeof chunk);
                    itemsXML = itemsXML + chunk;
                });
            resp.on("error",
                function (err) {
                    console.log("Got error: " + e.message);
                    res.status(err.status || 500);
                    res.render('error', {
                        message: err.message,
                        error: err
                    })
                });
            resp.on("end",
                function () {
                    console.log("RES : " + itemsXML)
                    xml2js.parseString(itemsXML,function(err,result){
                        if(err){
                            console.log(err);
                        }
                        res.status(202);
                        res.json(result);
                        for (var  i = 0 ; i<result.items.item.length ; ++i) {
                            console.log(result.items.item[i]);
                        }
                        console.log("PARSED RESULT : " + JSON.stringify(result));
                    });

                });
        });
}

router.get('/', function(req, res, next) {
    getAllItems(res);
});

router.get('/getAll', function(req, res, next) {
    getAllItems(res);
});

router.get('/notif/:item_id/', function(req, res, next) {
    getAllItems(res);
});

router.get('/listItems', function(req, res, next) {
    var item = new Item();
    item.find({}).exec(function(err, result) {
        if (!err) {
            console.log("Result: " + result);
        } else {
            console.log("Error Loading");
        }
    });
});


router.post('/collectNewItemState/:item_name', function( req, res, next){
    var app = require('../app');
    var io = app.io;
    io.emit('info', { msg : "ÇA C'EST BEAU!" } );

    var DAY_BEGIN = 'Bed OFF',
        DAY_END1 = 'Door OFF',
        DAY_END2 = 'Bed ON';

    var item = req.params.item_name,
        status = req.query.status,
        date = new Date(req.query.date),
        action = item + ' ' + status;
    console.log(action);

    tempAction.find(null, function( err, data){
        //console.log("Data: " + data);
        if(err) res.json(err);
        var myTempAction;
        if(!data[0] && action == DAY_BEGIN){
            myTempAction = new tempAction();
            myTempAction.actions = [action];
            myTempAction.absoluteTimes = [date];
            console.log('Begining of the Day.');

            myTempAction.save(function(err, b) {
                if (err)
                    res.json(err);
                else
                    console.log('item created!');
            });
        } else if (data[0]){
            myTempAction = data[0];
            myTempAction.actions.push(action);
            myTempAction.absoluteTimes.push(date);
            if( action == DAY_END1 || action == DAY_END2) {
                //TODO : PUT myTempAction in the real table.
                pathManager.addNewActions(myTempAction);
                
                tempAction.remove({_id: myTempAction._id}, function (err, data) {
                    if (err)
                        res.json(err);
                    else console.log('Suppr ok :');
                });
                console.log('END of the Day.');
            }else{
                console.log('Adding normal activity to temp.');
                predictions.predictAction(myTempAction,function(err,data){
                    if (err)
                    res.json(err);
                });
                myTempAction.save(function(err, b) {
                    if (err)
                        res.json(err);
                    else
                        console.log('item created!');
                });
            }
        }
    });
    next();
});



/**
*   Function responsible for getting and saving a new action taken in the house
*/
router.post('/collectNewItemState/:item_name', function( req, res, next){
    var name = req.params.item_name;
    var status = req.query.status;
    var date = req.query.date;
    console.log("Item name: " + name);
    console.log("Item status: " + status);
    console.log("Date: " + date);

    var item = new Item();
    item.name = name;
    item.status = status;
    item.date = date;
    item.save(function(err, itemObj) {
        if (err) {
            res.json(err);
        } else {
            var msg = "Ok! Item saved: " + itemObj;
            res.json(msg);
        }
    });
});


/**
*   Function responsible for listing all the actions done in the house by the user
*/
router.get('/listAllRecords', function(req, res, next) {
    Item.find({}, function(err, records) {
        if (err) {
            res.json(err);
        } else {
            res.json(records);
        }
    });
});


/**
*   Function responsible for sending a command to the OpenHAB server
*/
router.get('/changeItemState/:item_name/:item_state', function( req, res, next){
    var name = req.params.item_name;
    var state = req.params.item_state;
    var pathAddress = "/rest/items/" + name;
    var options = {
        host:'localhost',
        port:'8080',
        path: pathAddress,
        method: 'POST'
    }

    // Set up the request
    var post_req = http.request(options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
        });
        res.on('error', function(err){
            console.log(err);
        })
    });

    // Post the data
    post_req.write(state);

    // Execute the request
    post_req.end();

    res.json("Command " + state + " sent to " + name);
});

module.exports = router;

