var express = require('express');
var router = express.Router();

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var calendar = google.calendar('v3');

var SCOPES = ['https://www.googleapis.com/auth/calendar'];
var TOKEN_DIR = "SecurityTokens/";
var TOKEN_PATH = TOKEN_DIR + 'calendar-api-authToken.json';
//var oauth2Client;


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Authorization                                             #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

function authorize2(credentials,callback, params, res) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    console.log("HERE");
    fs.readFile(TOKEN_PATH, function(err, token) {
        if (err) {
            console.log("hello");
            getNewToken(oauth2Client,callback,params, res);
        } else {
            console.log("hello");
            oauth2Client.credentials = JSON.parse(token);
            callback(oauth2Client,params, res);
        }
    });
}

function getNewToken(oauth2Client, callback, params, res) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.question('Enter the code from that page here: ', function(code) {
        rl.close();
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client,params, res);
        });
    });
}

function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}


/****sdfqhjsd,hfkjdshkjfdkeizuglietjkl*******************************************ghjghjdsfgjksdfhkfjk*/
function postCalendarCreation(oauth2Client, ressource, res) {
    calendar.calendars.insert({
        auth: oauth2Client,
        resource: ressource
    }, function (err, something) {
        if (err) {
            res.errno(err);
            console.log(err);
        } else {
            console.log(something);
            res.json({'message' : something});
        }
    });
}
//************************************

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Event Creation                                            #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

function createCalendar(req, res) {
    var ressource = req.param('ressource');

    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        }
        // Authorize a client with the loaded credentials, then call the Calendar API.
        authorize2(JSON.parse(content), postCalendarCreation, ressource, res);
    });
}

/* GET users listing. */
router.get('/', function(req, res, next) {

    createCalendar(req, res);

});

/* GET users listing. */
router.post('/createCalendar', function(req, res, next) {
    createCalendar(req, res);

});
/*
router.get('/', function(req, res, next) {
    res.render('users', { title: 'Smart Home'});
});
*/


module.exports = router;
