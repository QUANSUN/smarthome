var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PathSchema   = new Schema({
    actions: [String],
    start_times: [Date],
    timings: [Schema.Types.Mixed],
    rank: Number
});

module.exports = mongoose.model('Path', PathSchema);