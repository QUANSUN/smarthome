var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var tempActionSchema   = new Schema({
    actions : [String],
    absoluteTimes : [Date]
});

module.exports = mongoose.model('tempAction', tempActionSchema);