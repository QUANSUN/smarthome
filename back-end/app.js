var express = require('express');
var socket_io = require( "socket.io" );
var app = express();
var io = socket_io();
app.io = io;
io.on("connection", function( socket )
{
    console.log( "A user connected" );
});

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/smartHome'); // connect to our database

var routes = require('./routes/index');
var users = require('./routes/users');
var items = require('./routes/items');
var createCalendar = require('./routes/createCalendar');
var itemsManager = require('./routes/itemsManager');
var pathManager = require('./routes/pathManager');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', routes);
app.use('/users/items', items);
app.use('/calendars', createCalendar);
app.use('/users', users);
app.use('/api/items', itemsManager);
app.use('/api/path', pathManager);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;