/**
 * Created by tom on 07/06/15.
 */

var googleAuth = require('google-auth-library');
var fs = require('fs');

var SCOPES = ['https://www.googleapis.com/auth/calendar'];
var TOKEN_DIR = "SecurityTokens/";
var TOKEN_PATH = TOKEN_DIR + 'calendar-api-authToken.json';


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// Authorization                                             #
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



function authorize(credentials,callback, req, res) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function(err, token) {
        if (err) {
            getNewToken(oauth2Client,callback,req, function(err, data){
                if(err){
                    res.json(err);
                }else{
                    res.json(data);
                }
            });
        } else {
            oauth2Client.credentials = JSON.parse(token);

            callback(oauth2Client,req, function(err, data){
                if(err){
                    res.json(err);
                }else{
                    res.json(data);
                }
            });
        }
    });
}

exports.authorize2 = function (credentials, callback, req, callback2){
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function(err, token) {
        if (err) {
            getNewToken(oauth2Client,callback,req, callback2);
        } else {
            oauth2Client.credentials = JSON.parse(token);

            callback(oauth2Client,req, callback2);
        }
    });
}

function getNewToken(oauth2Client, callback, req, callback2) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.question('Enter the code from that page here: ', function(code) {
        rl.close();
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                callback2(err, token);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client, req, callback2);
        });
    });
}

function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}

